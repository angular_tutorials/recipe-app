import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from '@angular/common/http';
// import { Http, Response } from '@angular/http';

import { map } from 'rxjs/operators';

import { RecipeService } from '../recipes/recipe.service';
import { Recipe } from '../recipes/recipe.model';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class DataStorageService {

  constructor(
    // private http: Http,
    private httpClient: HttpClient,
    private recipeService: RecipeService,
    private authService: AuthService
  ) { }

  storeRecipes() {
    const recipes = this.recipeService.getRecipes();
    const token = this.authService.getToken();
    // const header = new HttpHeaders().set('Authorization', 'Bearer blabla').append();
    // const params = new HttpParams().set('auth', token);

    // return this.httpClient.put(
    //   'https://ng-recipe-book-86ca0.firebaseio.com/recipes.json',
    //   recipes,
    //   {
    //     observe: 'body',
    //     params: params
    //     // headers: header
    //   }
    // );
    const req = new HttpRequest(
      'PUT',
      'https://ng-recipe-book-86ca0.firebaseio.com/recipes.json',
      recipes,
      // { reportProgress: true, params: params }
      { reportProgress: true }
    );

    return this.httpClient.request(req);
  }

  getRecipes() {
    const token = this.authService.getToken();

    this.httpClient.get<Recipe[]>('https://ng-recipe-book-86ca0.firebaseio.com/recipes.json?auth=' + token)
      // this.httpClient.get('https://ng-recipe-book-86ca0.firebaseio.com/recipes.json?auth=' + token, {
      //   observe: 'body',
      //   responseType: 'json'
      // })
      .pipe(
        map(
          (recipes) => {
            //   console.log(recipes);
            for (const recipe of recipes) {
              if (!recipe['ingredients']) {
                recipe['ingredients'] = [];
              }
            }
            return recipes;
            //   return [];
          }
        )
      )
      .subscribe(
        (recipes: Recipe[]) => {
          this.recipeService.setRecipes(recipes);
        }
      );
  }
}
