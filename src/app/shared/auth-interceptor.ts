import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { AuthService } from '../auth/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.authService.getToken();

    console.log('Intercepted!', req);
    // const copiedRequest = req.clone({headers: req.headers.append('', '')});
    const copiedRequest = req.clone({params: req.params.set('auth', token)});

    return next.handle(copiedRequest);
  }
}
