import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

import { AuthService } from './auth/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor (private authService: AuthService) {}

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyDPQZGxHs1U9MQnq6ZkuhDUQWVWE_nhtMY',
      authDomain: 'ng-recipe-book-86ca0.firebaseapp.com',
    });

    firebase.auth().onAuthStateChanged( (user) => {
      if (user) {
        user.getIdToken().then(
          (token: string) => this.authService.setToken(token)
        );
      }
    });
  }
}
