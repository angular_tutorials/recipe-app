import { Injectable } from '@angular/core';

import { Subject } from 'rxjs/';

import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable()
export class RecipeService {
  recipesChanged = new Subject<Recipe[]>();

  private recipes: Recipe[] = [
    new Recipe('A Test Recipe',
     'Simply a test',
     'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
    [
      new Ingredient('Meat', 3),
      new Ingredient('Pomems', 4),
    ]),
    new Recipe('Another Test Recipe',
     'Simply a test',
      'https://upload.wikimedia.org/wikipedia/commons/1/15/Recipe_logo.jpeg',
    [
      new Ingredient('Meat', 3),
      new Ingredient('Pomems', 4),
    ]),
    new Recipe('Lasagne',
     'Rezept für Lasagne',
      'https://upload.wikimedia.org/wikipedia/commons/6/6e/Tuna_lasagne_for_lunch%2C_January_2011.jpg',
    [
      new Ingredient('Zwiebel', 1),
      new Ingredient('Knoblauchzehe', 1),
      new Ingredient('Olivenöl, EL', 2),
      new Ingredient('Rindshackfleisch, Gramm', 500),
      new Ingredient('Tomaten, Gramm', 500),
    ]),
  ];

  constructor(private slService: ShoppingListService) { }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice());
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  getRecipes() {
    // By using slice() we get a copy of the array, thus preventing changes in the original array.
    // this is because arrays/objects ar passed by ref in javascript
    return this.recipes.slice();
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipesChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipesChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipesChanged.next(this.recipes.slice());
  }

}
